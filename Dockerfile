FROM node:alpine

LABEL authors="Valdas Mazrimas <valdas.mazrimas@gmail.com>"
WORKDIR /srv/apiproxy

COPY package*.json ./
RUN npm install --only=production

COPY ./src ./src

EXPOSE 8883

CMD ["npm", "start"]
