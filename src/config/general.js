const uuid = require('uuid/v4')

module.exports = {
  proxyPort: process.env.PROXY_PORT || 8883,
  proxyHost: process.env.PROXY_HOST || 'localhost',
  devLogger: process.env.DEV_LOGGER || true,
  corsOrigins: process.env.CORS_ALLOWED_ORIGINS || 'http://localhost:8885',
  amqpResponseQueue: process.env.RABBIT_RESPONSE_QUEUE || 'Api_Proxy_Response_Collector_Queue',
  proxyInstanceId: process.env.PROXY_INSTANCE_ID || uuid(),
  validateApiCallTopic: process.env.VALIDATE_API_CALL_TOPIC || 'ValidateAPICallRequest',
  validateApiCallTopicDone: process.env.VALIDATE_API_CALL_TOPIC_DONE || 'ValidateAPICallResponse',
  validateApiCallTimeout: process.env.VALIDATE_API_CALL_TIMEOUT || 5000,
  cookieName: process.env.API_COOKIE_NAME || 'crypto_aggregation_platform'
}
