module.exports = {
  connStr: process.env.RABBIT_CONN_STR || 'amqp://guest:guest@rabbitmq/dev_vhost',
  prettyConnStr: process.env.RABBIT_PRETTY_CONN_STR || 'amqp://*:*@rabbitmq/dev_vhost',
  retryInterval: process.env.RABBIT_RETRY_INTERVAL || 5000,
  timeout: process.env.RABBIT_RESPONSE_TIMEOUT || 5000
}
