module.exports = {
  status: {
    OK: 'OK',
    Fail: 'Fail',
    Timeout: 'Timeout'
  },
  code: {
    OK: 200,
    Fail: 401,
    Timeout: 504
  }
}
