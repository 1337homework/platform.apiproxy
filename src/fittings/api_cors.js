
const { generalConfig } = require('../config')
let CORS = require('cors')

// config options: https://www.npmjs.com/package/cors

module.exports = function create(fittingDef) {
  fittingDef.input.origin = generalConfig.corsOrigins.split(',')
  let middleware = CORS(fittingDef.input)

  return function cors(context, cb) {
    middleware(context.request, context.response, cb)
  }
}
