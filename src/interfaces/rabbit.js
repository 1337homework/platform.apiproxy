const { promisify } = require('util')
const moment = require('moment')
const { connect } = require('amqplib')
const { rabbit, generalConfig, constants } = require('../config')
const logger = require('../utils/logger')

const amqpObjects = {
  queueName: `${generalConfig.amqpResponseQueue}_${
    generalConfig.proxyInstanceId
  }`,
  routingKey: `${generalConfig.proxyInstanceId}.#`,
  servers: {
    [rabbit.connStr]: getRabbitServerObject()
  },
  waitingList: {}
}

/**
 * @param {Object} Consumables and producables for server
 * @returns {Object} Server rabbitmq server object
 */
function getRabbitServerObject({
  serverConsumables = [],
  serverProducables = []
} = {}) {
  const arrayToObjectProperties = (output = {}, topic) => {
    output[topic] = null
    return output
  }

  const obj = {
    address: rabbit.connStr,
    channel: null,
    connection: null,
    retries: 0,
    topics: {
      add: ({ consumable = [], producible = [] } = {}) => {
        if (consumable.length) {
          Object.assign(
            obj.topics.consumable,
            consumable.reduce(arrayToObjectProperties, {})
          )
        }
        if (producible.length) {
          Object.assign(
            obj.topics.producible,
            producible.reduce(arrayToObjectProperties, {})
          )
        }
      },
      consumable: serverConsumables.reduce(arrayToObjectProperties, {}),
      producible: serverProducables.reduce(arrayToObjectProperties, {})
    },
    log: logger.get(['amqp', rabbit.prettyConnStr])
  }

  return obj
}

/**
 * Add topics to amqp server
 * @param {Array} topics to add
 */
async function addTopics(topics) {
  const server = amqpObjects.servers[rabbit.connStr]

  if (server) {
    server.topics.add(topics)
  } else {
    amqpObjects.servers[rabbit.connStr] = getRabbitServerObject(topics)
  }
}

/**
 * Closes amqp connection
 */
async function closeConnection() {
  const server = amqpObjects.servers[rabbit.connStr]

  try {
    if (!server.connection) {
      return
    }

    server.connection.apiTerminated = true

    if (server.channel) {
      await stopConsummingInformation(server)
    }

    await server.connection.close()

    server.connection = null
  } catch (err) {
    server.log.error('An error occurred while terminating connection.', err)
  }
}

/**
 * @param {Object} server object
 * Unbindes consumer from queue
 */
async function stopConsummingInformation(server) {
  for (let topic in server.topics.consumable) {
    if (!server.topics.consumable.hasOwnProperty(topic)) {
      continue
    }

    try {
      await server.channel.unbindQueue(
        amqpObjects.queueName,
        topic,
        amqpObjects.routingKey
      )
    } catch (err) {
      server.log.error('Error while stopping consuming.', err)
    }
  }
}

/**
 * @param {Object} server object
 * @returns {any} result from close or nothing
 * Unbindes consumer from queue
 */
async function openConnection() {
  const server = amqpObjects.servers[rabbit.connStr]

  server.connection = null

  try {
    server.connection = await connect(rabbit.connStr)
    addTopics({ consumable: [generalConfig.validateApiCallTopicDone] })
  } catch (err) {
    server.log.error('Error while connecting to rabbitmq ', err)
    setTimeout(() => {
      openConnection()
    }, rabbit.retryInterval)
    return
  }

  server.connection.on('error', (err) => {
    let connection = this

    server.log.error('connection error.', err)
    connection.close()
  })

  server.connection.on('close', () => {
    let connection = this

    if (connection.apiTerminated) {
      return
    }

    setTimeout(() => {
      openConnection()
    }, rabbit.retryInterval)
  })

  server.log.info('Connected to rabbitmq, creating channel. ')

  try {
    server.channel = await createCommunicationChannel(server)
  } catch (err) {
    server.log.error('Error while creating communicaiton channel', err)
    return server.connection.close()
  }

  server.channel.on('close', () => {
    server.log.info('Rabbitmq channel closed.')
    server.channel = null
  })

  const { queue } = await server.channel.assertQueue(amqpObjects.queueName)

  if (queue) {
    await createConsumerExchanges(server)
    await createPublisherExchanges(server)

    server.channel.consume(queue, getMessageHandler(server))

    server.log.info(
      'Successfully conncted to Rabbitmq, listening for mesages.'
    )
  } else {
    throw new Error('Error when trying to create consumers and publishers')
  }
}

/**
 * @param {Object} server object
 * @returns {Object} channel or null
 * Creates communication channel
 */
async function createCommunicationChannel(server) {
  try {
    return await server.connection.createChannel()
  } catch (err) {
    server.log.error('Failed to open channel.', err)
    return null
  }
}

/**
 * @param {Object} server object
 *  Asserts consumer to exchanges
 */
async function createConsumerExchanges(server) {
  const topics = Object.keys(server.topics.consumable)

  let exchange

  for (let i = 0, { length } = topics; i < length; i++) {
    try {
      ({ exchange } = await server.channel.assertExchange(topics[i], 'topic', {
        durable: true
      }))
    } catch (err) {
      server.log.info(
        `Failed to assert exchanget for response topic: ${topics[i]}. ${err}`
      )
    }

    try {
      server.channel.bindQueue(
        amqpObjects.queueName,
        exchange,
        amqpObjects.routingKey
      )
    } catch (err) {
      server.log.info(
        `Failed to bind response queue to response topic: ${topics[i]}. ${err}`
      )
    }
  }
}

/**
 * @param {Object} server object
 * Asserts publishers to exchanges
 */
async function createPublisherExchanges(server) {
  const topics = Object.keys(server.topics.producible)

  for (let i = 0, { length } = topics; i < length; i++) {
    try {
      await server.channel.assertExchange(topics[i], 'topic', {
        durable: true
      })
    } catch (err) {
      server.log.error(
        `Failed to assert exchange for request topic: ${topics[i]}.${err}`
      )
    }
  }
}

/**
 * @param {Object} server object
 * @returns {Function} Message handler func
 * Builds a message handler and returns it
 */
function getMessageHandler(server) {
  const done = (message, success) =>
    success ? server.channel.ack(message) : server.channel.reject(message)

  return (originalMessage) => {
    try {
      const messageId = originalMessage.fields.routingKey.split('.')[1]
      const message = JSON.parse(originalMessage.content.toString('utf8'))

      if (!amqpObjects.waitingList[messageId]) {
        return done(originalMessage, true)
      }

      clearTimeout(amqpObjects.waitingList[messageId].timeout)

      amqpObjects.waitingList[messageId].callback(message)
      delete amqpObjects.waitingList[messageId]

      done(originalMessage, true)
    } catch (err) {
      server.log.info(`Error while finishing hadling rabbitmq msg ${err}`)
      done(originalMessage, false)
    }
  }
}

/**
 * @param {Object} message entity
 * @param {callback} callback to call after message is recieved back in response queue
 * @returns {any} result of callback function
 */
function publish(
  {
    message,
    messageId,
    topic,
    responseTopic,
    connectionString = rabbit.connStr,
    timeout = rabbit.timeout,
    method,
    requestId
  } = {},
  callback
) {
  const server = amqpObjects.servers[connectionString]
  const created = moment()

  if (!server) {
    throw new Error('No server errr...')
  }

  if (!server.channel) {
    throw new Error('No server channel errr...')
  }

  message.metadata = {
    created: created.format('MM/DD/YYYY HH:mm:ss'),
    correlationId: messageId,
    routingKey: `${generalConfig.proxyInstanceId}.${messageId}`,
    originalHttpMethod: method
  }

  server.channel.publish(
    topic,
    `${generalConfig.proxyInstanceId}.${messageId}`,
    Buffer.from(JSON.stringify(message)),
    {
      contentType: 'application/json',
      headers: {
        requestId,
        messageId
      },
      timestamp: created.unix()
    }
  )

  if (!responseTopic) {
    return callback()
  }

  amqpObjects.waitingList[messageId] = {
    callback,
    timeout: setTimeout(() => {
      server.log.error(`message - ${messageId} - timeout. Topic: ${topic}.`)
      delete amqpObjects.waitingList[messageId]

      callback({ status: constants.status.SERVER_TIMEOUT })
    }, timeout)
  }
}

module.exports = {
  addTopics,
  closeConnection,
  openConnection,
  publish: promisify(publish)
}
