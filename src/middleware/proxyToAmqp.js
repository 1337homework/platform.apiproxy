const uuid = require('uuid/v4')
const rabbitmq = require('../interfaces/rabbit')
const { constants } = require('../config')
const { setAuthToken, getAuthToken } = require('../utils/authToken')

const proxyToAmqp = (req, res, ctx, logger, next) => {
  if (res.headersSent || ctx.clientDisconnected) {
    return next()
  }

  logger.info('Forwarding request to defined AMQP topic.')

  const message = {
    path: req.path,
    authToken: getAuthToken(req)
  }

  Object.assign(message, req.body)

  logger.info('Forwarding request to defined rabbitmq topic.')

  rabbitmq.publish(
    {
      message,
      messageId: uuid(),
      topic: req.swagger.operation['x-request-rabbit-topic'],
      responseTopic: req.swagger.operation['x-response-rabbit-topic'],
      method: req.swagger.operation.method,
      timeout: req.swagger.operation['x-timeout']
    },
    (respMessage) => {
      logger.info('Response is recieved from defined response AMQP topic.')
      const responseStatus = respMessage.authStatus || respMessage.status

      if (
        responseStatus == constants.status.Timeout ||
        responseStatus == constants.status.Fail
      ) {
        return next(constants.code[responseStatus], respMessage, true)
      }

      res.set('Content-Type', 'application/json')

      req.swagger.result = {
        statusCode: respMessage.responseStatusCode || 200,
        headers: res.getHeaders(),
        body: respMessage
      }

      if (req.swagger.operation['x-logon-operation']) {
        setAuthToken(req, res, ctx, respMessage.authToken)
      }

      res.set('Cache-control', 'no-store')
      res.set('Pragma', 'no-cache')

      logger.info('Sending response to the client via HTTP.')

      return next(constants.code.OK, respMessage)
    }
  )
}

module.exports = proxyToAmqp
