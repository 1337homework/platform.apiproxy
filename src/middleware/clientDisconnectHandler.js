const clientDisconnectHandler = (req, res, ctx, logger, next) => {
  if (!res.headersSent && ctx.clientDisconnected) {
    // client disconnected before call completed can be used for
    // gathering data of all api endpoints request logs
    ctx.endTime = new Date()
    if (ctx.responsePayload != null) {
      ctx.responsePayload = Buffer.isBuffer(ctx.responsePayload)
        ? ctx.responsePayload.toString()
        : JSON.stringify(ctx.responsePayload)
    } else {
      ctx.responsePayload = Buffer.from('Connection was closed before end call.')
    }
    res.status(502)
    res.end()
    return next()
  }

  return next()
}

module.exports = clientDisconnectHandler

