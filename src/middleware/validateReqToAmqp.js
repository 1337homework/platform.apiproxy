const uuid = require('uuid/v4')
const rabbitmq = require('../interfaces/rabbit')
const { constants } = require('../config')
const { generalConfig } = require('../config')
const { setAuthToken, getAuthToken } = require('../utils/authToken')

const validateReqToAmqp = (req, res, ctx, logger, next) => {
  logger.info('Validation call started.')

  const message = {
    path: req.path
  }

  if (req.body.logonPackage) {
    Object.assign(message, {
      logonPackage: req.body.logonPackage
    })
  }

  const authToken = getAuthToken(req)

  if (authToken) {
    Object.assign(message, { authToken })
  }

  logger.info('Starting validate api call request to rabbitmq auth microservice.')

  rabbitmq.publish(
    {
      message,
      messageId: uuid(),
      topic: generalConfig.validateApiCallTopic,
      responseTopic: generalConfig.validateApiCallTopicDone,
      method: req.swagger.operation.method,
      timeout: generalConfig.validateApiCallTimeout
    },
    (respMessage) => {
      const responseStatus = respMessage.authStatus || respMessage.status

      if (respMessage.authToken && !req.swagger.operation['x-logon-operation']) {
        setAuthToken(req, res, ctx, respMessage.authToken)
      }

      switch (responseStatus) {
      case constants.status.OK:
        ctx.apiCallValidated = true
        logger.info('Validation completed successfully, calling next function in the pipe.')
        return next()
      case constants.status.Timeout:
        logger.error('Validation timeout')
        return next(constants.code.Timeout, respMessage, true)
      default:
        logger.error('Validation failed')
        return next(constants.code.Fail, respMessage, true)
      }
    }
  )
}

module.exports = validateReqToAmqp
