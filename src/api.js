const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const express = require('express')
const helmet = require('helmet')
const health = require('./middleware/health')
const registerSwagger = require('./swagger')

const instantiateApi = async () => {
  const api = express()

  api.use(helmet({
    hsts: false,
    frameguard: {
      action: 'deny'
    },
    noCache: true
  }))
  api.use(bodyParser.json())
  api.use(cookieParser())
  api.get('/health', health)

  await registerSwagger(api)

  return api
}

module.exports = instantiateApi
