const fs = require('fs')
const SwaggerExpress = require('swagger-express-mw')
const yaml = require('js-yaml')

let config = {
  appRoot: __dirname,
  swagger: yaml.safeLoad(fs.readFileSync(`${__dirname}/swagger/swagger.yaml`))
}

const registerSwagger = (api) => {
  return new Promise((resolve, reject) => {
    SwaggerExpress.create(config, (error, swaggerExpress) => {
      if (error) {
        reject(error)
      }

      resolve(swaggerExpress.register(api))
    })
  })
}

module.exports = registerSwagger
