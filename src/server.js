require('./utils/env')
const getApiInstance = require('./api')
const rabbitmq = require('./interfaces/rabbit')
const logger = require('./utils/logger').get()
const { generalConfig } = require('./config')

let httpServer

/**
 * Starts API Proxy server
 */
const start = async () => {
  try {
    const api = await getApiInstance()
    await rabbitmq.openConnection()

    const http = require('http')
    httpServer = http.createServer(api)
    httpServer.on('error', (err) => logger.info(`Api Server Failed ${err}`))
    httpServer.on('close', () => logger.info('Api Server Closed.'))
    httpServer.listen(
      generalConfig.proxyPort,
      generalConfig.proxyHost,
      () => logger.info(`Api Proxy listenning HTTP Requests. - ${generalConfig.proxyHost}:${generalConfig.proxyPort}`)
    )
  } catch (e) {
    throw new Error(e)
  }
}

start()
