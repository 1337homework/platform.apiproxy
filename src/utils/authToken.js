const { generalConfig } = require('../config')

const isIntegrationTestRunner = (req) => {
  return req.get('User-Agent') === 'request'
}

const validateAuthScheme = (token) => {
  try {
    const parts = token.split(' ')

    if (parts.length === 2) {
      const scheme = parts[0]
      const authToken = parts[1]

      if (/^Bearer$/i.test(scheme)) {
        return authToken
      }
    }
  } catch (e) { return null }

  return null
}

const getAuthToken = (req) => {
  const cookieToken = req.cookies[generalConfig.cookieName]

  if (cookieToken) {
    return cookieToken
  }

  try {
    const bearer = req.get('Authorization')
    const token = validateAuthScheme(bearer)
    return token
  } catch (e) {
    return null
  }
}

// This is not generic, if we would have
// more clients like mobile apps, terminal applications
// would then have to implement client identificaiton
const setAuthToken = (req, res, ctx, token) => {
  ctx.authToken = token

  if (isIntegrationTestRunner(req)) {
    res.set('Authorization', token)
    res.set('Access-Control-Expose-Headers', 'Authorization')
  } else {
    return res.cookie(generalConfig.cookieName, token, { httpOnly: true })
  }
}

module.exports = {
  setAuthToken,
  getAuthToken,
  validateAuthScheme
}
