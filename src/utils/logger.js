const { generalConfig } = require('../config')
const { createLogger, transports, format } = require('winston')
const { combine, timestamp, printf, prettyPrint } = format
const devLogFormat = printf((info) => `${info.timestamp} [${info.level}]: ${info.message}`)
const currentFormat = generalConfig.devLogger ? devLogFormat : prettyPrint()

const loggerObj = createLogger({
  format: combine(
    timestamp(),
    currentFormat
  ),
  transports: [
    new transports.Console()
  ]
})

const getLogHandler = (type, prefix) => {
  return (arg1, ...args) => {
    if (typeof arg1 === 'object') {
      loggerObj[type](`${prefix} - `, arg1, ...args)
    } else {
      loggerObj[type](`${prefix} - ${arg1}`, ...args)
    }
  }
}

const getLogger = (customPrefixes = []) => {
  const prefix = customPrefixes.length
    ? `${generalConfig.proxyInstanceId} - ${customPrefixes.join(' - ')}`
    : generalConfig.proxyInstanceId

  return {
    debug: getLogHandler('debug', prefix),
    info: getLogHandler('info', prefix),
    warn: getLogHandler('warn', prefix),
    error: getLogHandler('error', prefix)
  }
}

exports.get = getLogger
