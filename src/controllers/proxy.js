const uuid = require('uuid/v4')
const { constants } = require('../config')
const logger = require('../utils/logger').get()
const validateReqToAmqp = require('../middleware/validateReqToAmqp')
const proxyToAmqp = require('../middleware/proxyToAmqp')
const clientDisconnectHandler = require('../middleware/clientDisconnectHandler')

const formatError = (statusCode, body) => {
  const moreInfo = JSON.stringify(body)

  if (statusCode === constants.code.Fail) {
    return {
      status: 401,
      message: 'Unauthorized',
      moreInfo
    }
  }

  return {
    status: 500,
    message: 'Internal Server Error',
    moreInfo
  }
}

const pipe = (...fns) => (req, res, context) => {
  let fn = null

  req.on('close', () => {
    context.clientDisconnected = true
  })

  const next = (statusCode, body, isError = false) => {

    if (statusCode) {
      res.status(statusCode)
    }

    if (body) {
      const response = isError ? formatError(statusCode, body) : body

      context.responsePayload = response
      context.endTime = new Date()

      if (!context.clientDisconnected) {
        res.send(response)
      }
    }

    [fn, ... fns] = fns

    if (fn) {
      fn(req, res, context, logger, next)
    }
  }

  next()
}

const proxy = (req, res) => {

  let context = {
    requestId: uuid(),
    startTime: new Date(),
    originalHost: req.get('Host'),
    reqUrl: req.url
  }

  pipe(
    //validateClient,
    validateReqToAmqp,
    proxyToAmqp,
    clientDisconnectHandler
  )(req, res, context)
}

module.exports = {
  get: proxy,
  post: proxy
}
